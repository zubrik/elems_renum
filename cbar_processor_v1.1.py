def split_columns(line):
    result = []
    while len(line) > 0:
        result.append(line[:8])
        line = line[8:]
    return result

def read_file(filename):    
  lines = []
  with open(filename, "r") as f_in:
      for line in f_in.readlines():
          lines.append(split_columns(line.strip()))
  return lines

def write_file(filename, lines):
  with open(filename, "w") as f_out:
    for line in lines:
      f_out.write(''.join(line))
      f_out.write('\n')


def generate_id(cols):
    g1, g2 = cols[3], cols[4]
    g_min = min((int(g1), int(g2)))
    g_max = max((int(g1), int(g2)))

    if g1[:4] == g2[:4]:
        if g1[1] == '7':
            result = list(str(g_min + 1))            
            result[1] = '5'
            return ''.join(result)
        elif g1[1] == '1':
            result = list(str(g_min))
            result[1] = '5'
            return ''.join(result)
        else:
            raise Exception("4 digits are equal but [2] is not 1 or 7")
        
    else:
        if (g1[1] == '1' and g2[1] == '7') or (g1[1] == '7' and g2[1] == '1'):
            return str(g_max + 2)
        elif (g1[:2] == '31' and g2[:2] == '31'):
            if g1[3] == g2[3]:
                g_min = str(g_min)
                return "33%s%s%s%s%s%d" % (g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], g_min[7])
            else:
                result = str(g_max)
                return "33%s%s%s%s%s%d" % (result[4], result[5], result[2], result[3], g_min[6], g_min[7])                
        else:
            g_min = str(g_min)
            return "33%s%s%s%s%s%d" % (g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], g_min[7])
    
    g_min = str(g_min)
    return "33%s%s%s%s%s%d" % (g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], g_min[7])


def process_line(line):
  line[1] = generate_id(line)
  print (line)
  return line


if __name__ == "__main__":
  input_filename = "cbar_in.nas"
  output_filename = "cbar_out.nas"

  if input_filename == output_filename:
    raise Exception("Input file will be overridden!")

  lines = read_file(input_filename)
  result = []
  for (i, v) in enumerate(lines):
    if i % 2 == 0:
      result.append(process_line(v)) # writing every 1st line after processing
    else:
      result.append(v) # writing every 2nd line without changes

  write_file(output_filename, result)      