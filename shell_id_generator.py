"""
ID generation rules setup
"""
def generate_id(cols):
    pids = map(lambda x: int(x), cols[3:]) # oneline function
    g_min = str(min(pids))
    g_max = str(max(pids))
# rule 1 - first 4 digits are equal, the 2nd digit is either 1 or 7, the first 2 digits therefore 35
    if g_min[:6] == g_max[:6]: # Condition 1
        if g_min[1] == '7':
            result = list(str(int(g_min) + 1))            
            result[1] = '7'
            result[6] = '3'
            #result[7] = '0'
            return ''.join(result)
        else:
            raise Exception("4 digits are equal but [2] is not 1 or 7")
 # rule 2 - first 2 digits are either 31 and 37 or 31 and 31, the 2nd digit is either 1 or 7, the first 2 digits therefore 35       
    else:
        if (g_min[1] == '1' and g_max[1] == '7') or (g_min[1] == '7' and g_max[1] == '1'): # the 2nd digit is either 1 or 7 | Condition 2
            return str(g_max) # calulate the biggest of them
            """
        elif (g1[:2] == '31' and g2[:2] == '31'): # 31 and 31 rule
            if g1[3] == g2[3]:
                g_min = str(g_min) # calculate min
                return "33%s%s%s%s%s%d" % (g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], int(g_min[7]) + 1) # write in the following format
            else:
                result = str(g_max) # calculate max
                return "33%s%s%s%s%s%d" % (result[4], result[5], result[2], result[3], result[6], int(result[7]) + 1) # write in the following format                
        else:
            g_min = str(g_min)
            return "%s5%s%s%s%s%s%d" % (g_min[0], g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], int(g_min[7]) + 1) # write in the following format 
    """
    return "%s7%s%s%s%s3%d" % (g_min[0], g_min[2], g_min[3], g_min[4], g_min[5], int(g_min[7]) + 1) # write in the following format 
    

"""
Find and renumber duplicates
"""
def deduplicate_id(id, ids):
    if id in ids:
        new_id = deduplicate_id(str(int(id) + 1), ids) # add +1 to index in case ID already exists
        print("Conflicting ID: %s -> %s" % (id, new_id)) # service message
        return new_id
    else:
        ids.add(id) # add ID into list if there is no conflict
        return id

