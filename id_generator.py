def generate_id(cols):
    g1, g2 = cols[3], cols[4]
    g_min = min((int(g1), int(g2)))
    g_max = max((int(g1), int(g2)))
# rule 1 - first 4 digits are equal, the 2nd digit is either 1 or 7, the first 2 digits therefore 35
    if g1[:4] == g2[:4]:
        if g1[1] == '7':
            result = list(str(g_min + 1))            
            result[1] = '5'
            result[7] = '2'
            return ''.join(result)
        elif g1[1] == '1':
            result = list(str(g_min))
            result[1] = '5'
            return ''.join(result)
        else:
            raise Exception("4 digits are equal but [2] is not 1 or 7")
 # rule 2 - first 2 digits are either 31 and 37 or 31 and 31, the 2nd digit is either 1 or 7, the first 2 digits therefore 35       
    else:
        if (g1[1] == '1' and g2[1] == '7') or (g1[1] == '7' and g2[1] == '1'): # the 2nd digit is either 1 or 7
            return str(g_max + 2) # calulate the biggest of them and add 2
        elif (g1[:2] == '31' and g2[:2] == '31'): # 31 and 31 rule
            if g1[3] == g2[3]:
                g_min = str(g_min) # calculate min
                return "33%s%s%s%s%s%d" % (g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], int(g_min[7]) + 1) # write in the following format
            else:
                result = str(g_max) # calculate max
                return "33%s%s%s%s%s%d" % (result[4], result[5], result[2], result[3], result[6], int(result[7]) + 1) # write in the following format                
        else:
            g_min = str(g_min)
            return "%s3%s%s%s%s%s%d" % (g_min[0], g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], int(g_min[7]) + 1) # write in the following format 
    
    g_min = str(g_min)
    return "%s3%s%s%s%s%s%d" % (g_min[0], g_min[4], g_min[5], g_min[2], g_min[3], g_min[6], int(g_min[7]) + 1) # write in the following format 


def deduplicate_id(id, ids):
    if id in ids:
        new_id = deduplicate_id(str(int(id) + 1), ids)
        print("Conflicting ID: %s -> %s" % (id, new_id))
        return new_id
    else:
        ids.add(id)
        return id

