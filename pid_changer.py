import copy
from utils import load_properties, split_columns

if __name__ == "__main__":
  filename = "./data/L610G_Trup_v0.92_no_vs.nas"
  out_filename = "./data/out_replace.txt"
  out_props_filename = "./data/out_props.txt"

  props = {} # dict. props
  props_out = [] # array out props

  with open(out_filename, "w") as f_out:
    with open(filename, "r") as f:
      lines = f.readlines()
      props = load_properties(lines)

      for line in lines:
        line = line.strip()
        if line.startswith("CROD") or line.startswith("CBAR") or line.startswith("CTRIA3") or line.startswith("CQUAD4") or line.startswith("CSHEAR"):
          cols = split_columns(line)        
          pid = cols[2]
          cols[2] = cols[1]
          if pid not in props:
            raise Exception("PID %s is not found in properties" % pid)
          new_props = copy.deepcopy(props[pid])
          new_props[0][1] = cols[1]
          props_out.append(new_props)
          f_out.write(''.join(cols))
          f_out.write('\n')
        else:
          f_out.write(line)
          f_out.write('\n')

  with open(out_props_filename, "w") as f_props_out:
    for p in props_out:
      f_props_out.write("$%s Property Card\n" % p[0][0])
      for p_line in p:
        f_props_out.write(''.join(p_line))
        f_props_out.write('\n')


