from id_generator import generate_id, deduplicate_id

ids = set()

def split_columns(line):
    result = []
    while len(line) > 0:
        result.append(line[:8])
        line = line[8:]
    return result


# output file definition
def process(input_filename, output_filename):
    lines = []
    with open(input_filename, "r") as f:
        for line in f.readlines():
            lines.append(split_columns(line.strip()))

    with open(output_filename, "w") as f_out:
        for line in lines:       
            new_id = generate_id(line)
            line[1] = deduplicate_id(new_id, ids)
            out_line = ''.join(line)
            print(out_line)
            f_out.write(out_line)
            f_out.write('\n')


if __name__ == "__main__":
    process("data/crod_in.nas", "out/crod_out.nas")
    print("%d ids generated" % len(ids))
