def split_columns(line):
    result = []
    while len(line) > 0:
        result.append(line[:8])
        line = line[8:]
    return result

def generate_id(cols):
    g1, g2 = cols[3:]
    g_min = min((int(g1), int(g2)))
    g_max = max((int(g1), int(g2)))

    if g1[:4] == g2[:4]:
        if g1[1] == '7':
            result = list(str(g_min + 1))            
            result[1] = '5'
            return ''.join(result)
        elif g1[1] == '1':
            result = list(str(g_min))
            result[1] = '5'
            return ''.join(result)
        else:
            raise Exception("4 digits are equal but [2] is not 1 or 7")
        
    else:
        if (g1[1] == '1' and g2[1] == '7') or (g1[1] == '7' and g2[1] == '1'):
            return str(g_max + 2)
        elif (g1[:2] == '31' and g2[:2] == '31'):
            if g1[3] == g2[3]:
                g_min = str(g_min)
                return "33%s%s%s%s01" % (g_min[4], g_min[5], g_min[2], g_min[3])
            else:
                result = str(g_max)
                return "33%s%s%s%s01" % (result[4], result[5], result[2], result[3])                
        else:
            g_min = str(g_min)
            return "33%s%s%s%s01" % (g_min[4], g_min[5], g_min[2], g_min[3])
    
    g_min = str(g_min)
    return "33%s%s%s%s01" % (g_min[4], g_min[5], g_min[2], g_min[3])

def process(input_filename, output_filename):
    lines = []
    with open(input_filename, "r") as f:
        for line in f.readlines():
            lines.append(split_columns(line.strip()))

    with open(output_filename, "w") as f_out:
        for line in lines:            
            line[1] = generate_id(line)
            out_line = ''.join(line)
            print(out_line)
            f_out.write(out_line)
            f_out.write('\n')


if __name__ == "__main__":
    process("crod_temp1.nas", "crod_temp_out.nas")        