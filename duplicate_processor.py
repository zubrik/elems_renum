# -*- coding: utf-8 -*-   # russian text allowed
"""
parsing by 8 symbols
"""
def split_columns(line): 
    result = []
    while len(line) > 0:
        result.append(line[:8])
        line = line[8:]
    return result

def process(input_filename, output_filename):
    if input_filename == output_filename:
        raise Exception("file name is same") # stops method
    lines = []
    with open(input_filename, "r") as f:
        for line in f.readlines():
            lines.append(split_columns(line.strip()))

    with open(output_filename, "w") as f_out:
        for line in lines:            
            line[2] = line[1]
            out_line = ''.join(line) # склеиваем столбцы
            print(out_line)
            f_out.write(out_line)
            f_out.write('\n\r') # добавление новой строки в конце строки


if __name__ == "__main__":
    process("crod_out.nas", "crod_duplicates_out.nas")        