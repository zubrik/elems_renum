from shell_id_generator import generate_id, deduplicate_id # import id_generator and deduplication scripts

ids = set() # add empty set

def split_columns(line): # cols definition
    result = [] # add massive
    while len(line) > 0: # column must be not empty
        result.append(line[:8]) # split cols into 8 character length | add 8 sybol line
        line = line[8:] # cut first 8 symbols
    return result


# output file definition
def process(input_filename, output_filename):
    lines = [] #add massive
    with open(input_filename, "r") as f: #read file
        for line in f.readlines(): # return array of lines
            lines.append(split_columns(line.strip()))

    with open(output_filename, "w") as f_out:
        for line in lines:
            prefix = line[0].strip() # read 1st column
            if prefix in ["CQUAD4", "CTRIA3"]: # if CTRIA or CQUAD are in 1st column
                new_id = generate_id(line) # id generation
                line[1] = deduplicate_id(new_id, ids) # deduplication 
            out_line = ''.join(line) # put all cols together w/o gap or any character
            print(out_line) # service message
            f_out.write(out_line) # output data
            f_out.write('\n') # output data

"""
File settings
"""
if __name__ == "__main__":
    from sys import argv # argv magic - run script w/o opening the script
    if len(argv) < 3: # condition - not enough args
        print ("Usage: %s <INPUT_FILE> <OUTPUT_FILE>" % argv[0]) # service message in case name is too short
    else:
        process(argv[1], argv[2]) # run script
        print("%d ids generated" % len(ids)) # service message
