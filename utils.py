def split_columns(line): 
  result = []
  while len(line) > 0:
    result.append(line[:8])
    line = line[8:]
  return result


def load_properties(lines):
  props = {}
  for i in range(0, len(lines)):
    line = lines[i].strip()
    if line.startswith("PROD") or line.startswith("PSHELL") or line.startswith("PSHEAR"):
      cols = split_columns(line)
      props[cols[1]] = [cols]
    elif line.startswith("PBAR"):
      subset = lines[i: i+3]
      subset = map(lambda x: split_columns(x.strip()), subset)
      props[subset[0][1]] = subset
  return props


if __name__ == "__main__":
  filename = "./data/L610G_Trup_v0.91_no_vs.bdf"
  with open(filename, "r") as f:
    print(load_properties(f.readlines()))
  