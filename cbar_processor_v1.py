from id_generator import generate_id, deduplicate_id

ids = set()

def split_columns(line):
    result = []
    while len(line) > 0:
        result.append(line[:8])
        line = line[8:]
    return result

def read_file(filename):    
  lines = []
  with open(filename, "r") as f_in:
      for line in f_in.readlines():
          lines.append(split_columns(line.strip()))
  return lines

def write_file(filename, lines):
  with open(filename, "w") as f_out:
    for line in lines:
      f_out.write(''.join(line))
      f_out.write('\n')


def process_line(line):
  new_id = generate_id(line)
  line[1] = deduplicate_id(new_id, ids)
  print (line)
  return line


if __name__ == "__main__":
  input_filename = "cbar_in.nas"
  output_filename = "cbar_out.nas"

  if input_filename == output_filename:
    raise Exception("Input file will be overridden!")

  lines = read_file(input_filename)
  result = []
  for (i, v) in enumerate(lines):
    if i % 2 == 0:
      result.append(process_line(v)) # writing every 1st line after processing
    else:
      result.append(v) # writing every 2nd line without changes

  write_file(output_filename, result)      